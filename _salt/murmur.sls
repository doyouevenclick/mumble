mumble-server:
  pkg.installed: []
  service.running:
    - enable: true

/etc/mumble-server.ini:
  file.managed:
    - watch_in:
        - service: mumble-server
    - source: salt://mumble-server.ini

/etc/default/mumble-server:
  file.managed:
    - watch_in:
        - service: mumble-server
    - content: |
        # 0 = don't start, 1 = start
        MURMUR_DAEMON_START=1
        MURMUR_USE_CAPABILITIES=0

mumble.astraluma.com:
  acme.cert:
    - email: webmaster@astro73.com
    - group: mumble-server
    - require:
      - pkg: mumble-server
    - watch_in:
      - service: mumble-server

{% for dir in ['live', 'archive'] %}
/etc/letsencrypt/{{dir}}/mumble.astraluma.com:
  file.directory:
    - dir_mode: 755
    - file_mode: 644
    - user: root
    - group: root
    - watch_in:
      - service: mumble-server
{% endfor %}
