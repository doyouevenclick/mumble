mumble.astraluma.com:
  boto_route53.present:
    - value:
        - statichost-01.doyoueven.click
    - zone: astraluma.com.
    - record_type: CNAME
    - ttl: 600
